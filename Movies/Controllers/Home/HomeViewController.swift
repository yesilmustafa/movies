//  HomeViewController.swift
//  Movies

import RxSwift
import RxCocoa

class HomeViewController: BaseViewController {
    
    private let disposeBag = DisposeBag()
    private let throttleInterval = 0.4
    
    private var viewModel = HomeViewModel()
    
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var homeTableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        rxSetupSearchBar()
        rxSetupTableView()
    }
    
    //MARK: Rx Setup
    
    func rxSetupSearchBar() {
        let search = searchTextField.rx.text
        .orEmpty
        .throttle(throttleInterval, scheduler: MainScheduler.instance)
        .share(replay: 1)
        
         search.bind(to: viewModel.searchText)
        .disposed(by: disposeBag)
        
        let clickedValid = search
        clickedValid.map { !$0.hasSpecialCharacters() && $0.count > 1}
        .share(replay: 1)
        .bind(to: searchButton.rx.isEnabled)
        .disposed(by: disposeBag)
    }
    
    func rxSetupTableView(){
        
        homeTableView.register(UINib(nibName: XibNames.HomeMovieCell, bundle: nil), forCellReuseIdentifier: CellIdentifiers.home)
        homeTableView.tableFooterView = UIView(frame: .zero)
        
        viewModel.movies
            .asObservable()
            .bind(to: homeTableView.rx.items(cellIdentifier: CellIdentifiers.home, cellType: HomeMovieCell.self)) { (row, element, cell) in
                cell.setupWith(movie:element)
            }
        .disposed(by: disposeBag)
    
        
        homeTableView.rx.modelSelected(Movie.self)
        .subscribe(onNext: { [weak self] movie in
            
            self?.performSegue(withIdentifier: SegueIdentifiers.movieDetailSegue, sender: movie)

            if let selectedRowIndexPath = self?.homeTableView.indexPathForSelectedRow {
                self?.homeTableView.deselectRow(at: selectedRowIndexPath, animated: true)
            }
        })
        .disposed(by: disposeBag)
    }
    
    @IBAction func searchBarClicked(_ sender: Any) {
        self.startLoadingIndicator(withText: "Searching...")
        viewModel.searchMovie { (response) in
            self.stopLoadingIndicator()
        }
    }
    
    //MARK: Prepare for Segue
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let movie = sender as? Movie else { return }
        
        if segue.identifier == SegueIdentifiers.movieDetailSegue {
            if let viewController = segue.destination as? DetailViewController {
                viewController.detailMovie = movie
                viewController.title = movie.Title
            }
        }
    }
    
}
