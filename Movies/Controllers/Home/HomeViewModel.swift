//  HomeViewModel.swift
//  Movies

import RxCocoa
import RxSwift

class HomeViewModel: NSObject {
    
    var movies = Variable<[Movie]>([])
    var searchText = Variable<String>("")
    
    func searchMovie(completion: @escaping (Bool) -> ()) {
        
        self.movies.value.removeAll()
        
        ApiManager.shared.searchMovies(text: searchText.value) { (movie) in
            guard let checkMovie = movie else {
                completion(false)
                return
            }
            self.movies.value.append(checkMovie)
            
            completion(true)
        }
    }
    
}
