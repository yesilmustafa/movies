//  BaseViewController.swift
//  Movies

import UIKit
import JGProgressHUD

class BaseViewController: UIViewController {
    
    var loadingIndicator = JGProgressHUD(style: .dark)

    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

extension BaseViewController {
    
    // MARK: Loading Indicator Functions
    
    func stopLoadingIndicator() {
        loadingIndicator.dismiss()
    }
    
    func startLoadingIndicator(withText: String?) {
        loadingIndicator.textLabel.text = withText ?? ""
        loadingIndicator.show(in: self.view)
    }
    
}
