//  LaunchScreenViewController.swift
//  Movies

import UIKit
import Firebase


class LaunchViewController: BaseViewController {

    @IBOutlet weak var errorView: UIView!
    @IBOutlet weak var launchLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        checkInternetConnection()
    }
    
    func setupView() {
        self.launchLabel.text = RemoteConfig.remoteConfig().configValue(forKey: FirebaseRemoteConfigKeys.buttonText).stringValue ?? ""
    }
    
    func checkInternetConnection() {
        errorView.isHidden = true
        launchLabel.isHidden = false
        if Reachability.isConnectedToNetwork(){
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
               self.performSegue(withIdentifier: SegueIdentifiers.launchtoHomeSegue, sender: self)
            }
        }else{
            errorView.isHidden = false
            launchLabel.isHidden = true
        }
    }

}
