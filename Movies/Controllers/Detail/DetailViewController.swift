//
//  DetailViewController.swift
//  Movies
//
//  Created by Mustafa Yesil on 30.12.2018.
//  Copyright © 2018 Mustafa Yesil. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    
    @IBOutlet weak var movieImageView: UIImageView!
    @IBOutlet weak var movieTitleLabel: UILabel!
    @IBOutlet weak var genreLabel: UILabel!
    @IBOutlet weak var plotLabel: UITextView!
    
    
    private var detailViewModel: DetailViewModel?
    
    var detailMovie: Movie?

    override func viewDidLoad() {
        super.viewDidLoad()
        detailViewModel = DetailViewModel(movie: detailMovie!)
        setupViews()
    }
    
    func setupViews() {
        movieTitleLabel.text = detailViewModel?.mTitle
        genreLabel.text = detailViewModel?.mGenre
        plotLabel.text = detailViewModel?.mPlot
        movieImageView.loadImageUsingCacheWith(urlString: (detailViewModel?.mPoster)!)
    }
    
}
