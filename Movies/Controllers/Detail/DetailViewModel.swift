//
//  DetailViewModel.swift
//  Movies
//
//  Created by Mustafa Yesil on 30.12.2018.
//  Copyright © 2018 Mustafa Yesil. All rights reserved.
//

import UIKit

class DetailViewModel {
    
    private var movie: Movie
    
    init(movie: Movie) {
        self.movie = movie
    }
    
    var mTitle: String {
        return movie.Title ?? ""
    }
    
    var mGenre: String {
        return movie.Genre ?? ""
    }
    
    var mPlot: String {
        return movie.Plot ?? ""
    }
    
    var mPoster: String {
        return movie.Poster ?? ""
    }
    
    
    

}
