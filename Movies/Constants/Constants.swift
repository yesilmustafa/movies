//  Constants.swift
//  Movies

class Constants {
    
    static let shared = Constants()
    
    private init () {}
    
    let apiKey = "e98aa851"
    
    func searchApiURLWith(_ text: String) -> String {
        return "http://www.omdbapi.com/?t=\(text)&apikey=\(apiKey)"
    }
}

enum SegueIdentifiers {
    static let launchtoHomeSegue = "launchtoHomeSegue"
    static let movieDetailSegue = "movieDetailSegue"
}

enum CellIdentifiers {
    static let home = "HOME_CELL"
}

enum XibNames {
    static let HomeMovieCell = "HomeMovieCell"
}

enum FirebaseRemoteConfigKeys {
    static let buttonText = "buttonText"
}
