//
//  HomeMovieCell.swift
//  Movies
//
//  Created by Mustafa Yesil on 1.01.2019.
//  Copyright © 2019 Mustafa Yesil. All rights reserved.
//

import UIKit

class HomeMovieCell: UITableViewCell {
    
    @IBOutlet weak var movieImageView: UIImageView!
    @IBOutlet weak var movieTitleLabel: UILabel!
    @IBOutlet weak var genreLabel: UILabel!
    @IBOutlet weak var plotLabel: UITextView!
    
    func setupWith(movie: Movie) {
        movieTitleLabel.text = movie.Title ?? ""
        genreLabel.text = movie.Genre ?? ""
        plotLabel.text = movie.Plot ?? ""
        
        if let imageUrl = movie.Poster {
            movieImageView.loadImageUsingCacheWith(urlString: imageUrl)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
