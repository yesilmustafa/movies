
//
//  LButton.swift
//  Movies
//
//  Created by Mustafa Yesil on 1.01.2019.
//  Copyright © 2019 Mustafa Yesil. All rights reserved.
//

import UIKit

@IBDesignable
open class LButton: UIButton {
    
    @IBInspectable
    public var cornerRadius : CGFloat = 2.0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
   public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
}
