//
//  Movie.swift
//  Movies
//
//  Created by Mustafa Yesil on 30.12.2018.
//  Copyright © 2018 Mustafa Yesil. All rights reserved.
//

import ObjectMapper

class Movie: Mappable {
    
    var Title     : String?
    var Year      : String?
    var Rated     : String?
    var Director  : String?
    var Writer    : String?
    var Actors    : String?
    var Language  : String?
    var Country   : String?
    var Genre     : String?
    var Plot      : String?
    var Poster    : String?
    var Ratings   : [Rating]?
    var Metascore : String?
    var imdbRating: String?
    var imdbVotes : String?
    var imdbID    : String?
    var Types     : String?

 
    required init?(map: Map) { }
    
    func mapping(map: Map) {
        Title        <- map["Title"]
        Year         <- map["Year"]
        Rated        <- map["Rated"]
        Director     <- map["Director"]
        Writer       <- map["Writer"]
        Actors       <- map["Actors"]
        Language     <- map["Language"]
        Country      <- map["Country"]
        Genre        <- map["Genre"]
        Plot         <- map["Plot"]
        Poster       <- map["Poster"]
        Ratings      <- map["Ratings"]
        Metascore    <- map["Metascore"]
        imdbRating   <- map["imdbRating"]
        imdbVotes    <- map["imdbVotes"]
        imdbID       <- map["imdbID"]
    }

}

