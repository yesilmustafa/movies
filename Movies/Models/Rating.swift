//
//  Rating.swift
//  Movies
//
//  Created by Mustafa Yesil on 30.12.2018.
//  Copyright © 2018 Mustafa Yesil. All rights reserved.
//

import ObjectMapper

class Rating: Mappable {
    
    var Source : String?
    var Value  : String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        Source   <- map["Source"]
        Value    <- map["Value"]
    }
}
