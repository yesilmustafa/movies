//  ApiManager.swift
//  Movies

import Alamofire
import ObjectMapper

class ApiManager {
    
    static let shared = ApiManager()
    
    private init () {}
    
    func searchMovies(text: String, completion: @escaping (_ movies: Movie?) -> ()) {
        guard let url = URL(string: Constants.shared.searchApiURLWith(text)) else {
            completion(nil)
            return
        }
        Alamofire.request(url,
                          method: .get,
                          parameters: nil)
            .validate()
            .responseJSON { response in
                guard response.result.isSuccess else {
                    print("Error while fetching remote rooms:\(String(describing: response.result.error))")
                    completion(nil)
                    return
                }
                
                guard let movie = Mapper<Movie>().map(JSONObject:response.result.value) else {
                    print("Malformed data received from Movie service")
                    completion(nil)
                    return
                }
                
                completion(movie)
        }
    }

}
